(*

Compilado en MacOs Mojave y Devian 9 Stretch
Autores: Felipe Parra

Instrucciones de uso:
	- Se debe encontrar el archivo de texto con los números en la misma carpeta en que se encuentra el código
	- El archivo de texto debe contener solo el número por línea. (no deben haber espacios, tabuladores, etc. como el que viene en el archivo .zip).
	- Se debe acceder a la carpeta en que se encuentra el archivo "lab01.ml" a través de la terminal/cmd.
	- Introducir el siguiente comando en la terminal/cmd: ocaml lab01.ml
	- El programa mostrará a través de la terminal los resultados obtenidos en el laboratorio.

*)

(*Librería para mostrar por pantalla*)
open Printf

let getBit numero =
	ceil(log (float_of_int(numero)) /. log 2.0)
;;


(* Función que suma los elementos dentro de la lista de manera recursiva *)
let rec sumatoriaElementosLista l =
    match l with
    | [] -> 0
    | hd :: tl -> hd + sumatoriaElementosLista tl
;;
 (*FIN SUMA*)

 (*Imprimir lista, utilizada para el correcto debugging del programa*)

let rec print_list_int myList = match myList with
	| [] -> print_endline ""
	| head::body -> 
	begin
	print_int head;
	print_endline "";
	print_list_int body
	end
;;

(*Función que llena una nueva lista con el peso en bits de los números de otra lista.*)
let rec crearListaSinResta myList lines2= match myList with
	| [] -> ()
	| head::body ->
	begin
		lines2 := int_of_float(ceil(log (float_of_int(head)) /. log 2.0)) :: !lines2;
		crearListaSinResta body lines2;
	end
;;


(*2da parte de la función que simultáneamente realiza el algoritmo de ir restando el término, con el término anterior
  para luego transformarlo a bits y enviarlo a una nueva lista, ésta función lo hace de manera recursiva y no realiza
  resta en el término final del arreglo.*)
let rec crearListaConRestaRecursiva myList lines2 aux = match myList with
	| [] -> ()
	| head::body ->
	begin
		if body = [] then begin lines2 := int_of_float(ceil(log (float_of_int((abs head))) /. log 2.0)) :: !lines2; end
	 	else
		begin
			lines2 := int_of_float(ceil(log (float_of_int((abs (head - aux)))) /. log 2.0)) :: !lines2;
			(*print_int head;*)
			(*print_endline "";*)
			let aux = head in
			crearListaConRestaRecursiva body lines2 aux
		end
	end
;;


let rec crearListaConRestaRecursivaEntera myList lines2 aux = match myList with
	| [] -> ()
	| head::body ->
	begin
		if body = [] then begin lines2 := abs (head) :: !lines2; end
	 	else
		begin
			lines2 := (abs (head - aux)) :: !lines2;
			let aux = head in
			crearListaConRestaRecursivaEntera body lines2 aux
		end
	end
;;

let crearListaConRestaEntera myList lines2 = match myList with
	| [] -> ()
	| head::body ->
	begin
		lines2 := head :: !lines2;
		(*Auxiliar que guardará el valor en cabecera*)
		let aux = head in
		crearListaConRestaRecursivaEntera body lines2 aux
	end
;;

let crearListaConResta myList lines2= match myList with
	| [] -> ()
	| head::body ->
	begin
		lines2 := int_of_float(ceil(log (float_of_int(head)) /. log 2.0)) :: !lines2;
		print_endline "";
		(*Auxiliar que guardará el valor en cabecera*)
		let aux = head in
		crearListaConRestaRecursiva body lines2 aux
	end
;;

(* Función que se encargará de leer línea por línea el archivo de texto, traduciendo la cadena de string
   e irlo enviando a una lista en formato entero *)
let leerArchivo nombreArchivo listaEnteros = 
	let chan = open_in nombreArchivo in
	try
	  while true; do
	    listaEnteros := int_of_string(input_line chan) :: !listaEnteros;
	  done; !listaEnteros
	with End_of_file ->
	  close_in chan;
	  List.rev !listaEnteros;
;;


let rec crearLista64Bits listaEntrada lista64Salida espacioDisponible numOut = match listaEntrada with
	| [] -> ()
	| head::body ->
	begin
		if ( int_of_float(getBit(head)) <= espacioDisponible ) then
		begin
			let x = Int64.of_int(numOut) in
			let y = int_of_float(getBit(head)) in

			let a = Int64.shift_left x y in
			let b = (Int64.to_int(a)) + head in
			let c = espacioDisponible - (int_of_float(getBit(head))) in
			(* 

			Print utilizados para el correcto debugging del programa

 			printf("VALOR DE A = ");
			print_int (abs (Int64.to_int(a)));
			printf("\nEspacio disponible en bits: ");
			print_int (espacioDisponible);
			printf("\n");
			printf("Tamaño en bits del número que se agregará: ");
			print_int (int_of_float(getBit(head)));

			printf("\n\nNuevo numero creado luego del shift_left y agregarle la entrada:");
			print_int (b);
			printf("\n");

			printf ("\n");
			printf("Nuevo espacio disponible: ");
			print_int (c);
			printf("\n\n");
			*)
			crearLista64Bits body lista64Salida c b;
		end
		else
		begin
			lista64Salida := numOut :: !lista64Salida;
			(* 

			Prints utilizados para el correcto debugging del programa

			printf("\n¡¡¡ SE INGRESO UN NUMERO !! \n");
			printf("Nuevo espacio disponible: ");
			print_int (64);
			printf("\n");
			printf("Se llama la función y se comparará con :");
			print_int (head);*)
			crearLista64Bits body lista64Salida 64 head;
		end
	end
;;

let rec correccionConteoBits listaEntrada listaSalida= match listaEntrada with 
| [] -> ()
| head::body ->
begin
	if (int_of_float(getBit(head)) == 0) then
		begin
			listaSalida := 64 :: !listaSalida;
			(*Auxiliar que guardará el valor en cabecera*)
			correccionConteoBits body listaSalida
		end
	else
		begin
			listaSalida := int_of_float(getBit(head)) :: !listaSalida;
			(*Auxiliar que guardará el valor en cabecera*)
			correccionConteoBits body listaSalida
		end
end
;;

(*Funcion principal que realizará el llamado al resto de funciones y creará las distintas listas*)
let main() = 
	(* Lista con los números del txt *)
	let listaEnteros = ref[] in
	(* Lista con los bits resultantes de los números enteros*)
	let numSinResta = ref [] in
	(* Lista con los bits resultantes tras aplicar el algoritmo de la resta a los números enteros*)
	let numConResta = ref [] in
	(*Lista con los números Enteros*)
	let numConRestaEntera = ref [] in
	(*Lista con los tamaños en bits del laboratorio 02*)
	let num64bitsInt = ref [] in

	let nombreArchivo = "datos_steim.txt" in
		leerArchivo nombreArchivo listaEnteros;
		crearListaConResta !listaEnteros numConResta;
		crearListaSinResta !listaEnteros numSinResta;
		crearListaConRestaEntera !listaEnteros numConRestaEntera;

	(* Lista que contendrá los números de 64bits y los irá concatenando*)
	let num64bits = ref [] in
		crearLista64Bits !numConRestaEntera num64bits 64 0;

		(*Se calculan los bits luego de la utilización de los algoritmos para concatenar enteros de 64bits*)
		correccionConteoBits !num64bits num64bitsInt;

		(*Variables utilizadas para obtener los porcentajes respecto al tamaño de los distintos arreglos*)
		let sumSinRestas = (sumatoriaElementosLista !numSinResta) in
		let sumConRestas = (sumatoriaElementosLista !numConResta) in
		let sumEmpaquetado = (sumatoriaElementosLista !num64bitsInt) in
		
		let ahorroBits = abs (sumConRestas - sumSinRestas) in
		printf("1.- ");
		print_float (float_of_int(ahorroBits * 100 ) /.float_of_int(sumSinRestas));

		let ahorroBits2 = abs (sumSinRestas - sumEmpaquetado) in
		printf("		2.- ");
		print_float (float_of_int(ahorroBits2 * 100)/.float_of_int(sumSinRestas));
		printf("\n");
;;

main ()