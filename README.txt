instrucciones de uso:
	- Se debe encontrar el archivo de texto con los números en la misma carpeta en que se encuentra el código
	- El archivo de texto debe contener solo el número por línea. (no deben haber espacios, tabuladores, etc. como el que viene en el archivo .zip).
	- Se debe acceder a la carpeta en que se encuentra el archivo "lab01.ml" a través de la terminal/cmd.
	- Introducir el siguiente comando en la terminal/cmd: ocaml lab01.ml
	- El programa mostrará a través de la terminal los resultados obtenidos en el laboratorio.

Laboratorio 01 del ramo Comunicación de Computadores. 
Realizado por Felipe Parra y Fernando García.

______

Enunciado:

#TITLE: Laboratorio 1, Grupo 1.
#SUBTITLE: Nociones básicas.
#AUTHOR: B_GA

* Introducción

Para ciertos variedades de datos en la vida real, en particular, para aquellos
que pueden ser medidos frecuentemente y que la mayor parte de las veces no
varían mucho, podemos usar un sencillo algoritmo de compresión.

Supongamos que los datos están cómodamente descritos en algún tipo secuencial.
El algoritmo comprende recorrer la secuencia mientras se guarda el valor
absoluto de la diferencia entre los valores consecutivos. Por ejemplo:

   Si el conjunto de datos es representado en una lista en OCaml, como
   [x_1; x_2; x_3; x_4; ...; x_{i-2}; x_{i-1}; x_i]

   Debe producirse la siguiente lista:
   [x_1; |x_2-x_1|; |x_3-x_2|; |x_4-x_3|; ...; |x_{i-1}-x_{i-2}|; |x_i-x_{i-1}|, x_i]

* Laboratorio

Implementar el algoritmo. Como todavía no sabemos cómo empaquetar más de un
valor en una palabra, vamos a calcular cuánto podemos ahorrar en el caso de
set de datos real, que está contenido en esta misma carpeta, bajo el nombre
`datos_steim.txt`. Es decir, cargarán los datos del archivo, e irán
construyendo el arreglo que contiene los datos comprimidos. Una vez terminado,
van a calcular cuánto espacio se ahorra, calculando cuántos bits son
necesarios para representar el número en cada una de las posiciones.

La entrega es el código fuente del programa y el resultado de

  1. El cálculo de uso de bits de los valores originales.
  2. El cálculo de uso de bits de los valores comprimidos.

La entrega es en esta misma carpeta, de la forma descrita en `anuncios.txt`
el día lunes 5 de noviembre.